<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => '16cde5d64162023c76c637fcf66dfad06b686a93',
    'name' => 'bcosca/fatfree',
  ),
  'versions' => 
  array (
    'anandpilania/f3-validator' => 
    array (
      'pretty_version' => 'v1.2.5',
      'version' => '1.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee5a1f648a14200656765259ede8e51ac7da1698',
    ),
    'bcosca/fatfree' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => '16cde5d64162023c76c637fcf66dfad06b686a93',
    ),
    'bcosca/fatfree-core' => 
    array (
      'pretty_version' => '3.8.2',
      'version' => '3.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '774692ce7698904d3cb35bbd4f79376bb17eeddc',
    ),
  ),
);
