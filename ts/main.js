import bootstrap from 'bootstrap';
import 'animate.css';
$(document).ready(function() {


    if($('.slider-home').length > 0){
        let owlHome = $('.slider-home');
        owlHome.owlCarousel({
            loop: true,
            nav:false,
            responsive: {
                0: {
                    items: 1,
                    dots: false,
                    autoplay:true,
                    autoplayTimeout:1000,
                    autoplayHoverPause:true
                },
                768:{
                    items: 1,
                    dots: false
                }
            }
        });

        $('.sbd-next').click(function() {
            owlHome.trigger('next.owl.carousel');
        })
        $('.sbd-prev').click(function() {
            owlHome.trigger('prev.owl.carousel', [300]);
        })
    }
    if($('.slider-nosotros').length > 0){
        $('.slider-nosotros').owlCarousel({
            loop: true,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                    dots: false
                }
            }
        });
    }

    if($('.owl1').length > 0){
        $('.owl1').owlCarousel({
            loop: true,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                    dots: true
                }
            }
        });
    }

    if($('.owl2').length > 0){
        $('.owl2').owlCarousel({
            loop: true,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    margin: 100
                }
            }
        });
    }

    if($('.owl3').length > 0){
        $('.owl3').owlCarousel({
            loop: true,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                    dots: false,
                    autoplay:true,
                    autoplayTimeout:1000,
                    autoplayHoverPause:true
                }
            }
        });
    }

    if($('#contact').length > 0){
        let baseUrl = window.location.protocol + '//' + window.location.host;
        $('#contact').validate({
            rules: {
                name: {
                    required: true
                },
                business: {
                    required: true
                },
                phone: {
                    required: true
                },
                email: {
                    required: true
                },
                address1: {
                    required: true
                },
                address2: {
                    required: true
                },
                message: {
                    required: true
                },
                youhavebusiness: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Por favor, introduce tu nombre"
                },
                business: {
                    required: "Por favor, introduce tu empresa"
                },
                phone: {
                    required: "Por favor, introduce tu teléfono"
                },
                email: {
                    required: "Por favor, introduce tu correo electrónico"
                },
                address1: {
                    required: "Por favor, introduce tu dirección"
                },
                address2: {
                    required: "Por favor, introduce tu dirección"
                },
                message: {
                    required: "Por favor, introduce tu mensaje"
                },
                youhavebusiness: {
                    required: "Por favor, introduce tu respuesta"
                }
            },
            errorElement: 'small',
            errorPlacement: function(error, element) {
                //bootstrap 5
                error.addClass('invalid-feedback d-block');
                error.insertAfter(element);
            },
            highlight: function(element, errorClass, validClass) {
                // bootstrap 5
                $(element).addClass('is-invalid');
                $(element).removeClass('is-valid');
                $(element).removeClass('is-invalid');
            },
            submitHandler: function(form) {
                Swal.showLoading()
                fetch(baseUrl+'/api/sendmail', {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: 'POST',
                    body: JSON.stringify({
                        name: $('input[name="name"]').val(),
                        business: $('input[name="business"]').val(),
                        phone: $('input[name="phone"]').val(),
                        email: $('input[name="email"]').val(),
                        address1: $('input[name="address1"]').val(),
                        address2: $('input[name="address2"]').val(),
                        message: $('textarea[name="message"]').val(),
                        youhavebusiness: $('input[name="youhavebusiness"]').val()
                    })
                }).then(result => result.json())
                .then(data => {
                    reloadCode();
                    if (data.status == true){
                        form.reset();
                        Swal.close();
                        Swal.fire({
                            title: 'Mensaje enviado con exito!!',
                            icon: 'success',
                            showCloseButton: true,
                            focusConfirm: false,
                            showConfirmButton: false,
                            showCancelButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            backdrop: '#000000ba'
                        })
                    }
                })
            }
        });

        $(document).ready(function() {
            reloadCode();
        });
        $('#reload-code').click(function(e) {
            e.preventDefault();
            reloadCode();
        })
        
        function reloadCode() {
            let iframe = '<iframe src="'+baseUrl+'/api/re-captcha" height="35" width="135"></iframe>';
            $('#recaptcha').html(iframe);
        }
    }

});

if($('.counter').length > 0){
    $('.counter').each(function() {
        var $this = $(this),
            countTo = $this.attr('data-count');
    
        $({ countNum: $this.text()}).animate({
                countNum: countTo
            },
            {
                duration: 5000,
                easing:'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                    //alert('finished');
                }
    
            });
    
    });
}

